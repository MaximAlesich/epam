﻿using System;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            Module1 instance = new Module1();
            int[] swappedItems = instance.SwapItems(1, 2);
            int minimumValue = instance.GetMinimumValue(new[] { 10, 20, 1 });
        }


        public int[] SwapItems(int a, int b)
        {
            return new[] { b, a };
        }

        public int GetMinimumValue(int[] input)
        {

            int minValue = input[0];

            for (int i = 1; i < input.Length; i++)
            {
                if (input[i] < minValue)
                {
                    minValue = input[i];
                }
            }
            return minValue;
        }
    }
}
